# change DrawExtra number
POINTS=~/java-studenti/body.txt
CORRECT=~/java-studenti/correct-ones.txt
TARGET=target/classes
POL_OUT=pol-out.txt
POL_BIN=polygon.bin

for filename in x*; do
  
  if grep -q $filename "$CORRECT"; then # if already checked and correct, skip
    continue
  fi

  echo -en "$filename \t" >> $POINTS
  pushd $filename
  
  if [ -d "$TARGET" ]; then # compiled correctly => target dir exists
    pushd $TARGET
    java cz.muni.fi.pb162.project.demo.Draw 
    java cz.muni.fi.pb162.project.demo.DrawExtra09
    popd
    echo 'Diff pol-out'
    diff ~/jjava-cviciaci/pb162-seminar-project-private/$POL_OUT "$POL_OUT"

    pushd $TARGET
    echo 'Diff pol-bin'
    diff ~/jjava-cviciaci/pb162-seminar-project-private/$POL_BIN "$POL_BIN"
    if [ $? -ne 0 ]; then
      echo 'Draw not found' >> $POINTS
    else
      read -p "Was ok? [enter]/n  " yn
      case $yn in
         [Nn]* ) echo "Draw was wrong" >> $POINTS ;;
         * ) echo '*1' >> $POINTS; echo $filename >> $CORRECT ;;
      esac
      rm -rf target/
    fi
    popd
  else
    echo 'Build failed ' >> $POINTS
  fi
  popd
done
