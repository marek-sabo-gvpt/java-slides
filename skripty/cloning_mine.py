# run skript as: python3 cloning.py
# prerequisities: set SSH key, see https://gitlab.fi.muni.cz/pb162/pb162-course-info/wikis/fork#01-add-your-public-ssh-key-to-your-gitlab-profile 

from subprocess import call
from urllib.request import urlopen
import json
import subprocess, shlex

allProjects     = urlopen("https://gitlab.fi.muni.cz/api/v3/projects?per_page=100&private_token=L6h4DAjaMZVqxG8bQa44")
allProjectsDict = json.loads(allProjects.read().decode())
for thisProject in allProjectsDict: 
    try:
        url = thisProject['ssh_url_to_repo']
        if (url.endswith('pb162-seminar-project.git')):
            #print(url)
            if ('owner' in thisProject):
                username = thisProject['owner']['username']
            else:
                continue
            print(username)
            command  = shlex.split('git clone %s %s' % (url, username) )
            resultCode  = subprocess.Popen(command).wait()

    except Exception as e:
        print("Error on %s." % url)
call(["rm", "-r", "xsabo4/"])
# xfialov3
# don't forget to check number of repositories: ls -1 . | grep "x" | wc -l
