# run: ./check.sh xname
# change every week: pull public iteration, uncomment, DrawExtra number and remove correct-ones.txt file
POINTS=~/java-studenti/body.txt
CORRECT=~/java-studenti/correct-ones.txt
TARGET=target/classes
DRAW='Draw.java'
DRAW_EXTRA='DrawExtra09.java'
PUBLIC_PROJECT=../pb162-seminar-project-public/src/main/java/cz/muni/fi/pb162/project/demo
DEMO_DIR=src/main/java/cz/muni/fi/pb162/project/demo

if [ "$#" -ne 1 ]; then
  echo "Illegal number of parameters"
  echo "Must have 1 xname directory argument"
  exit
fi

if grep -q $1 "$CORRECT"; then # if already checked and correct, skip
  continue
fi

pushd $1 # go to xname dir

#git reset --hard
#git checkout develop # just to be sure
#git pull origin develop -X theirs

cp -r ../pb162-seminar-project-public/src/test/ src/
cp "$PUBLIC_PROJECT/$DRAW_EXTRA" "$DEMO_DIR/$DRAW_EXTRA"
cp "$PUBLIC_PROJECT/$DRAW" "$DEMO_DIR/$DRAW" 
mvn clean install -Dcheckstyle.fail=true  > "/tmp/$1" 2>&1

#if [ $? -ne 0 ]; then
#  echo "$1 has checkstyle/test problems" >> $POINTS
#  mvn clean install -Dcheckstyle.fail=true | grep "[ERROR]" > "/tmp/$1"
#   mvn clean install -Dcheckstyle.fail=false # > /dev/null 2>&1
#fi

if [ $? -ne 0 ]; then
  echo "$1 tests failed" 
  echo "$1 tests failed" >> $POINTS
  rm -rf $TARGET
fi

popd

