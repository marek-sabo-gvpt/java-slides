# one argument: iteration number
echo "iteration-0"$1
pushd pb162-seminar-project-public
git pull origin iteration-0$1

pushd src/main/java/cz/muni/fi/pb162/project/demo/
  ls
  DRAWEXTRA='DrawExtra0'$1'.java' 
  sed -i 's/\/\*[ \t]*$//' $DRAWEXTRA # remove /*
  tac $DRAWEXTRA | sed "1,5s/}//" | sed '1,5s/^[ \t]*\*\/[ \t]*$//' | tac > "DrawToFile.java"
popd

popd
